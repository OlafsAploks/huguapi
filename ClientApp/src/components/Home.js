import React from 'react';
import { connect } from 'react-redux';

const Home = props => (
  <div>
    <h1>Hello!</h1>
	<p>You are running this application in <b>{process.env.NODE_ENV}</b> mode.</p>
  </div>
);

export default connect()(Home);
