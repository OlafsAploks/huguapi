﻿import React from 'react';
import { connect } from 'react-redux';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

import BearActions from '../store/BearStore';
import './Styles/BearList.css';

class BearList extends React.Component {

	componentDidMount() {
		console.log('ComponentDidMount');
		this.props.getBearRequest();
	}

	onItemClicked = (item) => {
		console.log(item);
	}

	renderBears = () => {
		if (this.props.bears.length) {
			return (
				<ListGroup>
					{
						this.props.bears.map(bear => {
							return (
								<ListGroupItem
									key={bear.bearKey}
									onClick={this.onItemClicked}
								>
									{bear.factoryName}, {bear.bearKey}
								</ListGroupItem>
								)
						})
					}
				</ListGroup>
				)
		}

		return <p>Saraksts ir tukšs</p>
	}

	render() {
		return (
			<div className="bear-list-container" >
				<div>
					<h1>System bears</h1>
				</div>
				{ this.renderBears() }
			</div>
		);
	}
};

const mapStateToProps = (state) => {
	console.log(state);
	return {
		bears: state.bears.payload
	}
};

const mapDispatchToProps = (dispatch) => {
	console.log(BearActions)
	return {
		getBearRequest: () => dispatch(BearActions.getBearsRequest())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(BearList);