﻿import { call, put } from 'redux-saga/effects';
import { TYPES } from '../store/WeatherForecasts';
import { actionCreators } from '../store/WeatherForecasts';

export default function* getWeatherForecasts(action) {
	console.log(action);
	const startDateIndex = 5;
	const url = `api/SampleData/WeatherForecasts?startDateIndex=${startDateIndex}`;
	try {
		const response = yield call(promisifiedGetWeatherRequest, url);
		console.log('Response', response);
		const jsonData = yield call(promisifiedToJson, response);
		console.log('Response JSON', jsonData);
		yield put(actionCreators.receiveWeatherForecastsType({
			payload: jsonData,
			startDateIndex: startDateIndex
		}));
	} catch (e) {
		console.log('Promise rejection', e);
	}
}

function promisifiedToJson(data) {
	return new Promise(async (resolve, reject) => {
		const jsonData = await data.json();
		resolve(jsonData)
	});
}

function promisifiedGetWeatherRequest(url) {
	return new Promise(async (resolve, reject) => {
		const response = await fetch(url);
		console.log('RESPONSEEE', response);
		resolve(response);
	});
}