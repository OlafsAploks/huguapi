﻿import { takeLatest, takeEvery, all } from 'redux-saga/effects';
import API from '../services/API';

/* --------	Import types ---------*/
//import { TYPES } from '../store/WeatherForecasts';
import { BearTypes } from '../store/BearStore';
/* --------	Import sagas ---------*/
//import getWeatherForecasts from './WeatherSagas';
import getBearsRequest from './BearsSagas';


// Connecting types to sagas
export default function* rootSaga() {
	console.log(BearTypes);
	yield all([
		// some sagas only receive an action
		//takeLatest(TYPES.requestWeatherForecastsType, getWeatherForecasts),
		takeLatest(BearTypes.GET_BEARS_REQUEST, getBearsRequest, API.getAllBears)
		// some sagas receive extra parameters in addition to an action
		//takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api)
	])
}