﻿import { call, put } from 'redux-saga/effects';
import BearActions from '../store/BearStore';

export default function* getWeatherForecasts(url, action) {
	try {
		const response = yield call(fetch, url);
		if (response.status === 200) {
			const data = yield call(promisifiedToJson, response);
			console.log('Response JSON', data);
			yield put(BearActions.getBearsSuccess(data));
			return;
		}
		yield put(BearActions.getBearsFailure('Failed to return'));
	} catch (e) {
		console.log('Promise rejection', e);
	}
}

function promisifiedToJson(data) {
	return new Promise(async (resolve, reject) => {
		const jsonData = await data.json();
		resolve(jsonData)
	});
}

/*function promisifiedGetBearsRequest(url) {
	return fetch(url);
}
function promisifiedGetBearsRequest(url) {
	return new Promise(async (resolve, reject) => {
		const response = await fetch(url);
		console.log('Response', response)
		if (response.status === 200) {
			resolve(response.data);
		}
		reject(response);
	});
}*/