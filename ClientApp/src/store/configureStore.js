﻿import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga'

import rootSaga from '../sagas/index';

import * as BearStore from './BearStore';

export default function configureStore(history, initialState) {
	const reducers = {
		bears: BearStore.reducer
	};

	const sagaMiddleware = createSagaMiddleware();

	const middleware = [
		thunk,
		routerMiddleware(history),
		sagaMiddleware
	];

	// In development, use the browser's Redux dev tools extension if installed
	const enhancers = [];
	const isDevelopment = process.env.NODE_ENV === 'development';
	if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
		enhancers.push(window.devToolsExtension());
	}

	const rootReducer = combineReducers({
		...reducers,
		routing: routerReducer
  });

	const store = createStore(
		rootReducer,
		initialState,
		compose(applyMiddleware(...middleware), ...enhancers)
	);

	sagaMiddleware.run(rootSaga);
	return store;
}
