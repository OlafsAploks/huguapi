﻿import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
	getBearsRequest: null,
	getBearsSuccess: ['payload'],
	getBearsFailure: ['error'],
});

export const BearTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
	fetching: false,
	payload: [],
	error: ''
});
/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { searchString }) =>
	state.merge({
		fetching: true,
		payload: INITIAL_STATE.payload,
		error: INITIAL_STATE.error
	});

// successful api lookup
export const success = (state, action) => {
	console.log(action);
	const { payload } = action;
	return state.merge({ fetching: false, payload });
};
// Something went wrong somewhere.
export const failure = (state, action) =>
	state.merge({ fetching: false, error: action.error, payload: INITIAL_STATE.payload });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
	[Types.GET_BEARS_REQUEST]: request,
	[Types.GET_BEARS_SUCCESS]: success,
	[Types.GET_BEARS_FAILURE]: failure,
});