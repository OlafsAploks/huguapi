﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Models
{

	public class Bear
	{
		public int Id { get; set; }
		public string BearKey { get; set; }
		public string FactoryName { get; set; }
		//public bool Extension;
		//public DateTime CreatedAt { get; set; }

		public Bear() { }
		public Bear(string name, string key)
		{
			BearKey = key;
			FactoryName = name;
			//	Timestamp
			//CreatedAt = DateTime.UtcNow;
		}
	}
}
