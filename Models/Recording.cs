﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Models
{
    public class Recording
    {
		public int Id { get; set; }
		public string Location { get; set; }
        public bool Converted { get; set; }
        public bool Received { get; set; }
		public bool Listened { get; set; }
		public DateTime CreatedAt { get; set; }
		//	Fk
		public int BearId { get; set; }
	}
}
