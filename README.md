﻿## Business flow

User from his mobile app records a recording (fairytale, song, some other message) and sends it to the server. Server stores it converts it and on download request, it returns the converted recording.
Download request is made by a toy bear, that you give to your loved ones. Bear is connected to home Wifi and it fetches the newest not listened recording on server. This is the basic idea of the project.

## Converting

File converting is implemented as seperate service (we might even call it a microservice). Server runs a task in background, that checks for un-converted recordings. If it finds any, request to the converting service is made.


## Built With

* ASP.NET Core 2.1
* NUnit, NSubstitute (on test project)

## Authors

* **Olafs Aploks**

## NOTE - project is in a early development stage, not yet fight ready.