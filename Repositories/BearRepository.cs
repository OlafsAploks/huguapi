﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuguAPI.Models;
using System.Data.SqlClient;
using System.Text;
using HuguAPI.Services.Database;
using HuguAPI.Services.Database.Seeds;
using Microsoft.AspNetCore.Mvc;

namespace HuguAPI.Repositories
{
	public class BearRepository : IBearRepository
	{
		private IEntityMapper _entityMapper;
		public BearRepository(IEntityMapper entityMapper)
		{
			_entityMapper = entityMapper;
		}
		public Bear GetById(int bearId)
		{
			string query = @"SELECT Id, BearKey, FactoryName FROM dbo.Bear
							WHERE Id = @bearId";
			var dbq = new DataBaseQuery(query);
			dbq.AddParamWithValue("@bearId", bearId);
			var bear = dbq.ExecuteSingle();
			if (bear != null)
			{
				return _entityMapper.MapBear(bear);
			}
			return null;
		}

		public Bear GetByBearKey(string bearKey)
		{
			string query = @"SELECT Id, BearKey, FactoryName FROM dbo.Bear
							WHERE BearKey = @bearKey";
			var dbq = new DataBaseQuery(query);
			dbq.AddParamWithValue("@bearKey", bearKey);
			var res = dbq.ExecuteSingle();
			if (res != null)
			{
				return _entityMapper.MapBear(res);
			}
			return null;
		}

		private Bear MapBear (int id, string bearKey, string factoryName)
		{
			return new Bear
			{
				Id = id,
				BearKey = bearKey,
				FactoryName = factoryName
			};
		}

		public List<Bear> GetAllBears()
		{
			string query = @"SELECT Id, BearKey, FactoryName FROM dbo.Bear";
			var dbq = new DataBaseQuery(query);
			IDBResultsList res = dbq.Execute();
			List<Bear> bears = res.MapToList<Bear>(_entityMapper.MapBear);

			return bears;
		}

		public bool Add(Bear newBear)
		{
			if (newBear != null)
			{
				var command = new DataBaseCommand(@"INSERT INTO dbo.Bear (BearKey, FactoryName) VALUES(@BearKey, @FactoryName)");
				command.AddParamWithValue("@BearKey", newBear.BearKey);
				command.AddParamWithValue("@FactoryName", newBear.FactoryName);
				return command.Execute();
			}
			return false;
		}

		public Bear GetByFactoryName(string factoryName)
		{
			var query = new DataBaseQuery(@"SELECT * FROM BEAR WHERE FactoryName = @FactoryName");
			query.AddParamWithValue("@FactoryName", factoryName);
			var row = query.ExecuteSingle();
			if (row != null)
			{
				return _entityMapper.MapBear(row);
			}
			return null;
		}
	}
}
