﻿using HuguAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Repositories
{
    public interface IRecordingRepository
    {
        bool create(string location, int bearId);
        string getRecordingPathForDownload(string bearKey);
        bool setRecordingReceived(string path);
        bool setRecordingListened(string path);
        List<Recording> GetRecordingsNotConverted();
        void SetRecordingConverted(int Id);

    }
}
