﻿using HuguAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Repositories
{
    public class GenericRepository<TEntity> where TEntity : class
    {
		internal BearContext _context; // global to app
		internal DbSet<TEntity> _dbSet;

		public GenericRepository(BearContext context) //	injects app db context
		{
			_context = context;
			_dbSet = context.Set<TEntity>();
		}

		public virtual IEnumerable<TEntity> Get()
		{
			return _dbSet.ToList();
		}

		public virtual TEntity GetById(int id)
		{
			return _dbSet.Find(id);
		}

		public virtual void Insert(TEntity entity)
		{
			_dbSet.Add(entity);
		}

		public virtual void Delete(TEntity entityToDelete)
		{
			if (_context.Entry(entityToDelete).State == EntityState.Detached)
			{
				_dbSet.Attach(entityToDelete);
			}
			_dbSet.Remove(entityToDelete);
		}
		public virtual void Delete(int id)
		{
			Delete(_dbSet.Find(id));
		}

		public virtual void Update(TEntity entityToUpdate)
		{
			_dbSet.Attach(entityToUpdate);
			_context.Entry(entityToUpdate).State = EntityState.Modified;
		}

	}
}
