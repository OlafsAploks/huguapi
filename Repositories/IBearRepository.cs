﻿using HuguAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Repositories
{
    public interface IBearRepository
    {
		Bear GetById(int bearId);
		Bear GetByBearKey(string bearKey);
		Bear GetByFactoryName(string factoryName);
		List<Bear> GetAllBears();
		Boolean Add(Bear newBear);

	}
}
