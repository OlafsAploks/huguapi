﻿using HuguAPI.Models;
using HuguAPI.Services.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Repositories
{
	public class RecordingRepository : IRecordingRepository
	{
        private IEntityMapper _entityMapper;
		public RecordingRepository(IEntityMapper entityMapper)
        {
            _entityMapper = entityMapper;
        }
		public bool create(string location, int bearId)
		{
			if (bearId != 0 && location != null)
			{
				var command = new DataBaseCommand(
					@"INSERT INTO Recording (Location, BearId) VALUES (@Location, @BearId)"
				);
				command.AddParamWithValue("@BearId", bearId);
				command.AddParamWithValue("@Location", location);
				return command.Execute();
			}
			return false;
		}

		public string getRecordingPathForDownload(string bearKey)
		{
			string query = @"SELECT TOP 1 Recording.Location as Location FROM Recording
				LEFT JOIN Bear ON Recording.BearId = Bear.Id
				WHERE Bear.BearKey = @bearKey AND Received = 0
				ORDER BY Recording.CreatedAt ASC;";
			var dbq = new DataBaseQuery(query);
			dbq.AddParamWithValue("@bearKey", bearKey);
			var res = dbq.ExecuteSingle();
			if (res != null)
			{
				return res.GetString("Location");
			}
			return null;
		}

        public List<Recording> GetRecordingsNotConverted()
        {
            string query = @"SELECT * FROM dbo.Recording WHERE Converted = '0'";
            var dbq = new DataBaseQuery(query);
            IDBResultsList res = dbq.Execute();
            List<Recording> recordings = res.MapToList<Recording>(_entityMapper.MapRecording);
            return recordings;
        }

		public bool setRecordingReceived(string path)
		{
			if (path != null)
			{
				var command = new DataBaseCommand(
					@"UPDATE Recording SET Received = 1 WHERE Location = @path"
					);
				command.AddParamWithValue("@path", path);
				return command.Execute();
			}
			return false;
		}

		public bool setRecordingListened(string path)
		{
			if (path != null)
			{
				var command = new DataBaseCommand(
					@"UPDATE Recording SET Listened = 1 WHERE Location = @path"
					);
				command.AddParamWithValue("@path", path);
				return command.Execute();
			}
			return false;
		}

        public void SetRecordingConverted(int Id)
        {
            if (Id != 0)
            {
                var command = new DataBaseCommand(
                    @"UPDATE Recording SET Converted = 1 WHERE Id = @Id"
                    );
                command.AddParamWithValue("@Id", Id);
                command.Execute();
            }
        }
    }
}
