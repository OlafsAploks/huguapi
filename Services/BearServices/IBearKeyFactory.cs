﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services
{
    public interface IBearKeyFactory
    {
		string GetKey(int length);
    }
}
