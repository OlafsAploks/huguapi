﻿using HuguAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services
{
    public class BearFactory: IBearFactory
    {
		private const int KEY_LENGTH = 20;
		private IBearKeyFactory _bearKeyFactory;
		public BearFactory(IBearKeyFactory KeyFactory)
		{
			_bearKeyFactory = KeyFactory;
		}

		public Models.Bear createBear(string factoryName)
		{
			return new Models.Bear(factoryName, _bearKeyFactory.GetKey(KEY_LENGTH));
		}
	}
}
