﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services
{
    public class RandomFactory: IRandomFactory
    {
		private Random _random;
		public RandomFactory()
		{
			_random = new Random();
		}

		public int getNextInt(int Max)
		{
			return _random.Next(Max);
		}
	}
}
