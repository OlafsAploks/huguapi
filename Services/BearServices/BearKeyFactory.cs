﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services
{
    public class BearKeyFactory: IBearKeyFactory
    {
		private IRandomFactory _randomFactory;
		public BearKeyFactory(IRandomFactory randomFactory)
		{
			_randomFactory = randomFactory;
		}

		string IBearKeyFactory.GetKey(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string(Enumerable.Repeat(chars, length)
			  .Select(str =>
				str[_randomFactory.getNextInt(str.Length)]
				)
				.ToArray());
		}
	}
}
