﻿namespace HuguAPI.Services
{
	public interface IBearFactory
    {
		Models.Bear createBear(string factoryName);
    }
}
