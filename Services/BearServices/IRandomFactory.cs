﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services
{
    public interface IRandomFactory
    {
		int getNextInt(int Max); // 0 to max
    }
}
