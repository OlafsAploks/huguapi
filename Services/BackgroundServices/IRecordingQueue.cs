﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.BackgroundServices
{
    public interface IRecordingQueue
    {
        void Enqueue(Models.Recording recording);
        void Enqueue(List<Models.Recording> recordings);
        bool TryDequeue(out Models.Recording recording);
        Models.Recording Peek();
        bool IsEmpty();
    }
}
