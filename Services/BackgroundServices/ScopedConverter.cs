﻿using HuguAPI.Repositories;
using HuguAPI.Services.Recording;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.BackgroundServices
{
    public class ScopedConverter : IScopedConverter
    {
        private IRecordingRepository _recordingRepository;
        private IFileServices _fileServices;
        private IRecordingConverter _recordingConverter;
        private IRecordingQueue _recordingQueue;
        public ScopedConverter(IRecordingRepository recordingRepository,
            IFileServices fileServices,
            IRecordingConverter recordingConverter,
            IRecordingQueue recordingQueue)
        {
            _recordingRepository = recordingRepository;
            _fileServices = fileServices;
            _recordingConverter = recordingConverter;
            _recordingQueue = recordingQueue;
        }
        public void DoWorkAsync()
        {
            Debug.WriteLine("Helloo from bakckground");
            List<Models.Recording> recordings = _recordingRepository.GetRecordingsNotConverted();
            _recordingQueue.Enqueue(recordings);
            //  Split the converting part into another Scoped class
            //  Bothe classes should run async from one another
            //  Make Queue thread safe
            Models.Recording recording;
            while (_recordingQueue.TryDequeue(out recording))
            {
                var resp = _recordingConverter.Convert(recording);
            }

        }
    }
}
