﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuguAPI.Models;

namespace HuguAPI.Services.BackgroundServices
{
    public class RecordingQueue: IRecordingQueue
    {
        private Queue<Models.Recording> _recordings;

        public RecordingQueue()
        {
            _recordings = new Queue<Models.Recording>();
        }

        public bool TryDequeue(out Models.Recording recording)
        {
            return _recordings.TryDequeue(out recording);
        }

        public void Enqueue(Models.Recording recording)
        {
            if (!_recordings.Contains(recording))
            {
                _recordings.Enqueue(recording);
            }
        }

        public Models.Recording Peek()
        {
            return _recordings.Peek();
        }

        //  Enqueue many recordings at once
        public void Enqueue(List<Models.Recording> recordings)
        {
            foreach (var recording in recordings)
            {
                if (!_recordings.Contains(recording))
                {
                    _recordings.Enqueue(recording);
                }
            }
        }

        public bool IsEmpty()
        {
            return _recordings.Count == 0;
        }
    }
}
