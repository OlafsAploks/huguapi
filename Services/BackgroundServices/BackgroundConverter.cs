﻿using HuguAPI.Repositories;
using HuguAPI.Services.Recording;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HuguAPI.Services.BackgroundServices
{
    public class BackgroundConverter: IHostedService, IDisposable
    {
        private Timer _timer;
        public BackgroundConverter(IServiceProvider services)
        {
            Services = services;
            
        }

        public IServiceProvider Services { get; }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(10));
            Debug.WriteLine("Initializing BackgroundConverter");
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            Debug.WriteLine("---- Do Work BackgroundConverter");
            using (var scope = Services.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IScopedConverter>();
                scopedProcessingService.DoWorkAsync();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Debug.WriteLine("Stoping async BackgroundConverter");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Debug.WriteLine("Disposing BackgroundConverter");
            _timer?.Dispose();
        }
    }
}
