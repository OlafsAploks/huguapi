﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.BackgroundServices
{
    interface IScopedConverter
    {
        void DoWorkAsync();
    }
}
