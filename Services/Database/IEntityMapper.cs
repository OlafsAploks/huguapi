﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
    public interface IEntityMapper
    {
		Models.Bear MapBear(IDBResultsRow row);
		Models.Recording MapRecording(IDBResultsRow row);
    }
}
