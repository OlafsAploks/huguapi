﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
    public interface IDBResultsRow
    {
		int GetInt(string fieldName);
		string GetString(string fieldName);
		bool GetBool(string fieldName);
		DateTime GetDateTime(string fieldName);
		void Add(string fieldName, object value);
		int GetCount();
		//bool HasValue()
    }
}
