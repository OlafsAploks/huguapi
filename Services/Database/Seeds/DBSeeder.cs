﻿using HuguAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database.Seeds
{
	public class DBSeeder : IDBSeeder
	{
		private IBearFactory _bearFactory;
		private IBearRepository _bearRepository;
		public DBSeeder(IBearFactory bearFactory, IBearRepository bearRepository)
		{
			_bearFactory = bearFactory;
			_bearRepository = bearRepository;
		}
		public void Seed()
		{
			new BearTableSeeder(_bearFactory, _bearRepository).Seed();
		}
	}
}
