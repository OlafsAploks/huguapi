﻿using HuguAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database.Seeds
{
    public class BearTableSeeder: IDBTableSeeder
    {
		private IBearFactory _bearFactory;
		private IBearRepository _bearRepository;
		public BearTableSeeder(IBearFactory f, IBearRepository rep)
		{
			_bearFactory = f;
			_bearRepository = rep;

		}
		public void Seed()
		{
			int count = 10;
			string values = @"('aAbBcCdDeEfFgGhH', 'HUGU-T1')";
			for (int i = 2; i <= count; i++)
			{
				var bear = _bearFactory.createBear("HUGU-T" + i.ToString());
				values += ", ('" + bear.BearKey + "','" + bear.FactoryName + "')";
			}

			try
			{
				new DataBaseCommand(@"DELETE FROM Bear").Execute();
				var str = @"INSERT INTO Bear (BearKey, FactoryName) VALUES " + values;
				new DataBaseCommand(@"INSERT INTO Bear (BearKey, FactoryName) VALUES " + values).Execute();
			}
			catch (Exception e)
			{
				Console.Out.WriteLine("BearTableSeeder Failed: " + e.Message);
				throw;
			}
		}
    }
}
