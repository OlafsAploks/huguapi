﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database.Seeds
{
    public interface IDBSeeder
    {
		void Seed();
    }
}
