﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuguAPI.Models;

namespace HuguAPI.Services.Database
{
	public class EntityMappingLibrary : IEntityMapper
	{
		public Models.Bear MapBear(IDBResultsRow row)
		{
			return new Models.Bear
			{
				Id = row.GetInt("Id"),
				BearKey = row.GetString("BearKey"),
				FactoryName = row.GetString("FactoryName")
			};
		}

		public Models.Recording MapRecording(IDBResultsRow row)
		{
			return new Models.Recording
			{
				Id = row.GetInt("Id"),
				Location = row.GetString("Location"),
                Converted = row.GetBool("Converted"),
				Received = row.GetBool("Received"),
				Listened = row.GetBool("Listened"),
				CreatedAt = row.GetDateTime("CreatedAt"),
				BearId = row.GetInt("BearId")
			};

		}
	}
}
