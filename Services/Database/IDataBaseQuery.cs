﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
    public interface IDataBaseQuery
    {
		IDBResultsRow ExecuteSingle();
		IDBResultsList Execute();
		void AddParamWithValue(string paramName, object value);
    }
}
