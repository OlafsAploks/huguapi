﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
	public class DataBaseQuery : IDataBaseQuery
	{
		private readonly string _connectionString;
		private readonly string _query;
		private Dictionary<string, object> _params;
		public DataBaseQuery(string query)
		{
			_query = query;
			_params = new Dictionary<string, object>();
			_connectionString = @"Server=localhost\SQLEXPRESS;Database=Hugu;Trusted_Connection=True;";
		}
		public void AddParamWithValue(string paramName, object value)
		{
			_params.Add(paramName, value);
		}

		private void AttachProperties(SqlCommand cmd)
		{
			foreach (KeyValuePair<string, object> entry in _params)
			{
				cmd.Parameters.AddWithValue(entry.Key, entry.Value);
			}
		}

		public IDBResultsRow ExecuteSingle()
		{
			try
			{
				using (var connection = new SqlConnection(_connectionString))
				{
					using (var sqlCommand = new SqlCommand(_query, connection))
					{
						AttachProperties(sqlCommand);
						connection.Open();
						using (SqlDataReader reader = sqlCommand.ExecuteReader())
						{
							var results = new DBResultsRow();
							var resultsValues = new object[reader.FieldCount];
							while (reader.Read())
							{
								reader.GetValues(resultsValues);
								for (int i = 0; i < reader.FieldCount; i++)
								{
									results.Add(reader.GetName(i), resultsValues[i]);
								}
							}
							if (results.GetCount() == 0)
							{
								return null;
							}
							return results;
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
                throw e;
				return null;
			}
		}

		public IDBResultsList Execute()
		{
			try
			{
				using (var connection = new SqlConnection(_connectionString))
				{
					using (var sqlCommand = new SqlCommand(_query, connection))
					{
						AttachProperties(sqlCommand);
						connection.Open();
						using (SqlDataReader reader = sqlCommand.ExecuteReader())
						{
							var dbResults = new DBResultsList();
							while (reader.Read())
							{
								var row = new DBResultsRow();
								var resultsValues = new object[reader.FieldCount];
								reader.GetValues(resultsValues);
								for (int i = 0; i < reader.FieldCount; i++)
								{
									row.Add(reader.GetName(i), resultsValues[i]);
								}
								dbResults.Add(row);
							}
							return dbResults;
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
                throw e;
				return null;
			}
		}
	}
}
