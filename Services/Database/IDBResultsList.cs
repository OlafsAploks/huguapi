﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
    public interface IDBResultsList
    {
		void Add(IDBResultsRow row);
		List<T> MapToList<T>(Func<IDBResultsRow, T> mapFn);
		int GetCount();
    }
}
