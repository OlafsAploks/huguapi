﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
	public class DBResultsList: IDBResultsList
	{
		private List<IDBResultsRow> _results;

		public DBResultsList ()
		{
			_results = new List<IDBResultsRow>();
		}

		public void Add(IDBResultsRow row)
		{
			_results.Add(row);
		}

		public int GetCount()
		{
			return _results.Count();
		}

		public List<T> MapToList<T>(Func<IDBResultsRow, T> mapFn)
		{
			var mappedList = new List<T>();
			foreach (var row in _results)
			{
				mappedList.Add(mapFn(row));
			}
			return mappedList;
		}
	}
}
