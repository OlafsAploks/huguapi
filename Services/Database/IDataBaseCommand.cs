﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
    public interface IDataBaseCommand
    {
		bool Execute();
		void AddParamWithValue(string paramName, object value);
	}
}
