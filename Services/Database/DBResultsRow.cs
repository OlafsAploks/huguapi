﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
	public class DBResultsRow : IDBResultsRow
	{
		private Dictionary<string, object> _values;
		public DBResultsRow()
		{
			_values = new Dictionary<string, object>();
		}
		public void Add(string fieldName, object value)
		{
			_values.Add(fieldName, value);
		}

		public bool GetBool(string fieldName)
		{
			return (bool)_values[fieldName];
		}

		public int GetCount()
		{
			return _values.Count;
		}

		public DateTime GetDateTime(string fieldName)
		{
			return (DateTime)_values[fieldName];
		}

		public int GetInt(string fieldName)
		{
			return (int)_values[fieldName];
		}

		public string GetString(string fieldName)
		{
			return (string)_values[fieldName];
		}

		public object GetValueFromKey(string fieldName)
		{
			return _values[fieldName];
		}
	}
}
