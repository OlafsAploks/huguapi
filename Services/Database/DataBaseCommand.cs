﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Database
{
	public class DataBaseCommand : IDataBaseCommand
	{
		private readonly string _cmd;
		private readonly string _connectionString;
		private Dictionary<string, object> _params;
		public DataBaseCommand(string cmd)
		{
			_cmd = cmd;
			_params = new Dictionary<string, object>();
			_connectionString = @"Server=localhost\SQLEXPRESS;Database=Hugu;Trusted_Connection=True;";

		}

		public void AddParamWithValue(string paramName, object value)
		{
			_params.Add(paramName, value);
		}

		public bool Execute()
		{
			try
			{
				using (var connection = new SqlConnection(_connectionString))
				{
					connection.Open();
					using (var sqlCommand = new SqlCommand(_cmd, connection))
					{
						AttachProperties(sqlCommand);
						sqlCommand.ExecuteNonQuery();
					}
					return true;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("-------EXCEPTION: " + e.ToString());
				return false;
			}
		}

		private void AttachProperties(SqlCommand cmd)
		{
			foreach (KeyValuePair<string, object> entry in _params)
			{
				cmd.Parameters.AddWithValue(entry.Key, entry.Value);
			}
		}
	}
}
