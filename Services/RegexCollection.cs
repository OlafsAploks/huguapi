﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HuguAPI.Services
{
    public class RegexCollection
    {
		private Regex _BearFactoryNameRegex;
		public RegexCollection()
		{
			_BearFactoryNameRegex = new Regex(@"^(HUGU-\d{4})$");
		}

		public Regex getBearFactoryNameRegex()
		{
			return _BearFactoryNameRegex;
		}


    }
}
