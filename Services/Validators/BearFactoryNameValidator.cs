﻿using HuguAPI.Models;
using HuguAPI.Repositories;
using HuguAPI.Services;
using HuguAPI.Services.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HuguAPI.Services.Bear
{
    public class BearFactoryNameValidator: IBearFactoryNameValidator
    {
		private Regex _reg;
		private IBearRepository _bearRepository;
		public BearFactoryNameValidator (RegexCollection regCollection, IBearRepository br)
		{
			_reg = regCollection.getBearFactoryNameRegex();
			_bearRepository = br;
		}

		public bool validate(string FactoryName)
		{
			if(FactoryName == null)
			{
				throw new System.ArgumentNullException();
			}
			else
			{
				if (_reg.IsMatch(FactoryName))
				{
					var bear = _bearRepository.GetByFactoryName(FactoryName);
					if (bear != null)
					{
						throw new System.ArgumentException("FactoryName already registered");
					}
					return true;
				}
				else
				{
					throw new System.ArgumentException("Did not match the patern", "FactoryName");
				}
			}
		}
    }
}
