﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Bear
{
    public interface IBearFactoryNameValidator
    {
		bool validate(string FactoryName);
    }
}
