﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace HuguAPI.Services.Recording
{
    public class FileServices: IFileServices
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private IRecordingNameFactory _recordingNameFactory;

		public FileServices(IRecordingNameFactory rnf, IHostingEnvironment hostingEnvironment)
		{
			_recordingNameFactory = rnf;
            _hostingEnvironment = hostingEnvironment;
		}
		public string AddRecording(IFormFile file)
		{
			const string FOLDERNAME = "recordings";
			string webRootPath = _hostingEnvironment.WebRootPath;
			string newPath = Path.Combine(webRootPath, FOLDERNAME);
			if (!Directory.Exists(newPath))
			{
				Directory.CreateDirectory(newPath);
			}
			if (file.Length > 0)
			{
				string extension = Path.GetExtension(file.FileName);
				string fileName = _recordingNameFactory.getName(20) + extension;
				string fullPath = Path.Combine(newPath, fileName);
				using (var stream = new FileStream(fullPath, FileMode.Create))
				{
					file.CopyTo(stream);
				}
				return fullPath;
			}
			return null;
		}

        public bool AddConvertedRecording(byte[] content, string filename)
		{
            try
            {
                const string FOLDERNAME = "convertedrecordings";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, FOLDERNAME);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (content.Length > 0)
                {
                    string fullPath = Path.Combine(newPath, filename);
                    using (var fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(content, 0, content.Length);
                    }
                    //System.IO.File.WriteAllBytes(newPath, content);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
	}
}
