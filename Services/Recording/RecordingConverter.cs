﻿using HuguAPI.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace HuguAPI.Services.Recording
{
    public class RecordingConverter : IRecordingConverter
    {
        private IHostingEnvironment _hostingEnvironment;
        private IRecordingRepository _recordingRepository;
        private IFileServices _fileServices;

        public RecordingConverter(IHostingEnvironment hostingEnvironment, IRecordingRepository recordingRepository, IFileServices fileServices)
        {
            _hostingEnvironment = hostingEnvironment;
            _recordingRepository = recordingRepository;
            _fileServices = fileServices;
        }

        async public Task<string> Convert(Models.Recording recording)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://104.129.5.43/");
                    byte[] data = await System.IO.File.ReadAllBytesAsync(recording.Location);
                    ByteArrayContent byteArrayContent = new ByteArrayContent(data);

                    MultipartFormDataContent multiPartDataContent = new MultipartFormDataContent();
                    multiPartDataContent.Add(byteArrayContent, "voicerecording", Path.GetFileName(recording.Location));

                    var result = await client.PostAsync("api/recording/convert", multiPartDataContent);
                    if (result.IsSuccessStatusCode)
                    {
                        byte[] content = await result.Content.ReadAsByteArrayAsync();
                        var name = Path.GetFileNameWithoutExtension(recording.Location);
                        name += ".wav";
                        _fileServices.AddConvertedRecording(content, name);
                        Debug.WriteLine("Recording converted successfully, Id = " + recording.Id.ToString());
                        _recordingRepository.SetRecordingConverted(recording.Id);
                        return "Ok";
                    }

                    return "Converting error";
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }
        /*
         byte[] data;
         using (var br = new BinaryReader(file.OpenReadStream()))
         {
         data = br.ReadBytes((int)file.OpenReadStream().Length);
         }*/
    }
}