﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Recording
{
	public class RecordingNameFactory : IRecordingNameFactory
	{
		private IRandomFactory _randomFactory;
		public RecordingNameFactory(IRandomFactory rf)
		{
			_randomFactory = rf;
		}
		public string getName(int length)
		{
			const string chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
			return new string(Enumerable.Repeat(chars, length)
			  .Select(str =>
				str[_randomFactory.getNextInt(str.Length)]
				)
				.ToArray());
		}
	}
}
