﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Recording
{
    public interface IFileServices
    {
		string AddRecording(IFormFile file);
		bool AddConvertedRecording(byte[] content, string filename);

	}
}
