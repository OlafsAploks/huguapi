﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Recording
{
    public interface IRecordingConverter
    {
        Task<string> Convert(Models.Recording recording);
    }
}
