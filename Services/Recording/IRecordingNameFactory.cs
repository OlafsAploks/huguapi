﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HuguAPI.Services.Recording
{
    public interface IRecordingNameFactory
    {
		string getName(int length);
    }
}
