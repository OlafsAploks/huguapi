using System;
using HuguAPI.Models;
using HuguAPI.Repositories;
using HuguAPI.Services;
using HuguAPI.Services.BackgroundServices;
using HuguAPI.Services.Bear;
using HuguAPI.Services.Database;
using HuguAPI.Services.Database.Seeds;
using HuguAPI.Services.Recording;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HuguAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			var connection = @"Server=localhost\SQLEXPRESS;Database=Hugu;Trusted_Connection=True;";
			// Entity framework??? Remove it
			services.AddDbContext<BearContext>(options => options.UseSqlServer(connection));

			// Add all custom services
			AddServices(services);

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }

		private void AddServices(IServiceCollection services)
		{
			services.AddTransient<IEntityMapper, EntityMappingLibrary>();

			services.AddTransient<IBearRepository, BearRepository>((ctx) =>
			{
				var mappingService = ctx.GetRequiredService<IEntityMapper>();
				return new BearRepository(mappingService);
			});

			services.AddTransient<IRecordingNameFactory, RecordingNameFactory>((ctx) =>
			{
				var rf = ctx.GetRequiredService<IRandomFactory>();
				return new RecordingNameFactory(rf);
			});
			services.AddTransient<IFileServices, FileServices>((ctx) =>
			{
				var recf = ctx.GetRequiredService<IRecordingNameFactory>();
                var hostingEnv = ctx.GetRequiredService<IHostingEnvironment>();
				return new FileServices(recf, hostingEnv);
			});
			services.AddTransient<IRecordingRepository, RecordingRepository>((ctx) => 
            {
                var em = ctx.GetRequiredService<IEntityMapper>();
                return new RecordingRepository(em);
            });
            services.AddTransient<IRecordingConverter, RecordingConverter>();


			services.Add(new ServiceDescriptor(typeof(IRandomFactory), new RandomFactory()));
			services.AddTransient<IBearKeyFactory, BearKeyFactory>((ctx) =>
			{
				IRandomFactory svc = ctx.GetRequiredService<IRandomFactory>();
				return new BearKeyFactory(svc);
			});
			services.AddTransient<IBearFactory, BearFactory>((ctx) =>
			{
				IBearKeyFactory svc = ctx.GetRequiredService<IBearKeyFactory>();
				return new BearFactory(svc);
			});
			services.AddTransient<IBearFactoryNameValidator, BearFactoryNameValidator>((ctx) =>
			{
				var svc = new RegexCollection(); // SHOULD REGISTER FIRST
				var bearRep = ctx.GetRequiredService<IBearRepository>();
				return new BearFactoryNameValidator(svc, bearRep);
			});

            //  Background services
            services.AddSingleton<IRecordingQueue, RecordingQueue>(); // Queue for unconverted recordings
            services.AddHostedService<BackgroundConverter>();
            services.AddScoped<IScopedConverter, ScopedConverter>((ctx) =>
            {
                var recordingRepository = ctx.GetRequiredService<IRecordingRepository>();
                var fileServices = ctx.GetRequiredService<IFileServices>();
                var recordingConverter = ctx.GetRequiredService<IRecordingConverter>();
                var recordingQueue = ctx.GetRequiredService<IRecordingQueue>();
                return new ScopedConverter(recordingRepository,
                    fileServices,
                    recordingConverter,
                    recordingQueue);
            });

            // SEEDER
            services.AddTransient<IDBSeeder, DBSeeder>((ctx) =>
			{
				var bearFact = ctx.GetRequiredService<IBearFactory>();
				var bearRep = ctx.GetRequiredService<IBearRepository>();
				return new DBSeeder(bearFact, bearRep);
			});
		}
	}
}
