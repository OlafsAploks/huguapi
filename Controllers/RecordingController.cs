﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using HuguAPI.Repositories;
using HuguAPI.Services.Database;
using HuguAPI.Services.Recording;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HuguAPI.Controllers
{
	[Route("[controller]/[action]")]
	public class RecordingController : Controller
    {
		private IFileServices _fileServices;
		private IRecordingRepository _recordingRepository;
		private IBearRepository _bearRepository;
		public RecordingController (IFileServices fs, IRecordingRepository recordingRep, IBearRepository bRep)
		{
			_fileServices = fs;
			_recordingRepository = recordingRep;
			_bearRepository = bRep;
		}

		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[HttpPost, DisableRequestSizeLimit]
		public async Task<ActionResult> Create(string bearKey, [FromServices] IRecordingConverter converter)
        {
			try
			{
                Debug.WriteLine("UPLOADING FILE");
				var file = Request.Form.Files[0];
				if (file == null)
				{
					return BadRequest("No file supplied");
				}
				var bear = _bearRepository.GetByBearKey(bearKey);
				if (bear == null)
				{
					return BadRequest("Invalid bear key");
				}
				var path = _fileServices.AddRecording(file);
				if (path == null)
				{
					return BadRequest("Could not upload file");
				}
				if (_recordingRepository.create(path, bear.Id))
				{
                    return Ok("Success");
				}
				return BadRequest("DB error");
			}
			catch (Exception ex)
			{
				return BadRequest("Upload Failed: " + ex.Message);
			}
        }

		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[ProducesResponseType(404)]
		[HttpPost]
		public IActionResult GetByKey (string bearKey)
		{
			if (bearKey == null)
			{
				return Content("Key not present");
			}
				var bear = _bearRepository.GetByBearKey(bearKey);
				var path = _recordingRepository.getRecordingPathForDownload(bearKey);
				if (path == null)
				{
					return NotFound();
				}
			if (_recordingRepository.setRecordingReceived(path))
			{
				Stream stream = new FileStream(path, FileMode.Open);
				return File(stream, "application/octet-stream");
			}
			return BadRequest("DB error");
		}
    }
}