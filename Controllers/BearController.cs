﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HuguAPI.Models;
using HuguAPI.Repositories;
using HuguAPI.Services;
using HuguAPI.Services.Bear;
using HuguAPI.Services.Database.Seeds;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace HuguAPI.Controllers
{
	[Route("[controller]/[action]")]
	public class BearController : Controller
	{
		private readonly IBearRepository _bearRepository;
		private readonly IBearFactory _bearFactory;
		private readonly IBearFactoryNameValidator _bearNameValidator;
		private readonly ILogger<BearController> _logger;

		public ActionResult index()
		{
			return View("~/ClientApp/public/index.html");
		}
		public BearController(ILogger<BearController> logger, IBearRepository rep, IBearFactory factory, IBearFactoryNameValidator nameValidator)
		{
			_bearRepository = rep;
			_bearFactory = factory;
			_bearNameValidator = nameValidator;
			_logger = logger;
		}

		public IActionResult GetAll([FromServices]IBearRepository rep)
		{
			try
			{
				List<Bear> items = _bearRepository.GetAllBears();
				return Ok(items);
			}
			catch (Exception e)
			{
				return Ok(e);
			}
		}
		[HttpPost]
		public IActionResult GetById(int id)
		{
			var item = _bearRepository.GetById(id);
			if (item == null)
			{
				return NotFound();
			}
			return Ok(item);
		}

		[HttpGet("{id}", Name = "GetByKey")]
		[ProducesResponseType(200, Type = typeof(Bear))]
		[ProducesResponseType(404)]
		public IActionResult GetByKey(string id)
		{
			Console.WriteLine(id);
			var item = _bearRepository.GetByBearKey(id);
			if (item == null)
			{
				return NotFound();
			}
			return Ok(item);
		}

		[HttpPost]
		public IActionResult Create(string factoryName)
		{
			Console.WriteLine(factoryName);
			try
			{
				_bearNameValidator.validate(factoryName);
				Bear newBear = _bearFactory.createBear(factoryName);
				if(_bearRepository.Add(newBear))
				{
					return Ok(newBear);
				}
				return Ok(false);
			}
			catch (Exception e)
			{
				if (e is ArgumentNullException)
				{
					return BadRequest(e.Message.ToString());
				}
				else if (e is ArgumentException)
				{
					return BadRequest(e.Message.ToString());
				}
				return BadRequest();
			}
		}

		public IActionResult Seed([FromServices]IDBSeeder sdr)
		{
			Console.Out.WriteLine("LOG");
			try
			{
				sdr.Seed();
				return Ok("DB Seeding completed successfully");
			}
			catch (Exception)
			{	
				return StatusCode(500, "Seeding failed");
			}
		}
	}
}
